import * as PIXI from 'pixi.js';
import {
	WebGLRenderer,
	Container
} from 'pixi.js';

export default () => {
	const renderer = new WebGLRenderer(800, 600);
	const stage = new Container();

	let bunny = null;

	// load the texture we need
	PIXI.loader.add('bunny', 'img/bunny.png').load(function (loader, resources) {
		// This creates a texture from a 'bunny.png' image.
		bunny = new PIXI.Sprite(resources.bunny.texture);

		// Setup the position and scale of the bunny
		bunny.position.x = 400;
		bunny.position.y = 300;
		bunny.anchor.set(0.5);

		bunny.scale.x = 1;
		bunny.scale.y = 1;

		// Add the bunny to the scene we are building.
		stage.addChild(bunny);

		// kick off the animation loop (defined below)
		animate();
	});

	function animate() {
		// start the timer for the next animation loop
		requestAnimationFrame(animate);

		// each frame we spin the bunny around a bit
		bunny.rotation += 0.01;

		// this is the main render call that makes pixi draw your container and its children.
		renderer.render(stage);
	}

	document.getElementById('app').appendChild(renderer.view);
};
