module.exports = {
	files: {
		javascripts: {
			joinTo: {
				'vendor.js': /^(?!app)/,
				'app.js': /^app/
			}
		},
		stylesheets: {
			joinTo: {
				'app.css': /^app/,
				'vendor.css': /^(bower_components|node_modules|vendor)/
			}
		}
	},

	plugins: {
		babel: {
			babelrc: true,
			ignore: [
				/^(node_modules|bower_components|vendor)/,
				'app/legacyES5Code/**/*'
			]
		},
		sass: {
			mode: 'native'
		}
	}
};
